import React from "react";
import {
  StyleSheet,
  View
} from "react-native";

/**
 * Application to add elements to a market list
 * It display all the items in a list below the input form
 */
export default function App() {
  return (
      <View style={styles.container}>
      </View>
  );
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: "#fff",
    alignItems: "center",
    justifyContent: "center",
  }
});
